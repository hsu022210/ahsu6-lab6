package cs601;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs601.hotelapp.HotelData;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;

/**
 * Created by hsu022210 on 10/30/16.
 */

/**
 * A simple web server that sends the same response for each GET request (an
 * html page that says Hello, friends!)
 *
 */

public class JettyHttpServer {

    public static final int PORT = 5000;

    public static void main(String[] args) throws Exception {
        Server server = new Server(5000);

        HotelData data = new HotelData();
        data.loadHotelInfo("input/hotels200.json");
        data.loadReviews(Paths.get("input/reviews"));

        // We could also setup the connector component explicitly
        //ServerConnector connector = new ServerConnector(server);
        //connector.setHost("localhost");
        //connector.setPort(PORT);
        //server.addConnector(connector);

//        ServletHandler handler = new ServletHandler();
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");
        context.setAttribute("hotelData", data);
        context.addServlet(HotelInfoServlet.class, "/hotelInfo");
        context.addServlet(ReviewsServlet.class, "/reviews");

        // when the user goes to http://localhost:8080/hello, the get request is
        // going to go to a HelloServlet
//        handler.addServletWithMapping(HotelInfoServlet.class, "/hotelInfo");
//        handler.addServletWithMapping(ReviewsServlet.class, "/reviews");

//        server.setHandler(handler);
        server.setHandler(context);

        server.start();
        server.join();
    }
}
