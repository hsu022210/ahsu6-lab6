package cs601;

/**
 * Created by hsu022210 on 10/30/16.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs601.hotelapp.HotelData;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringEscapeUtils;

/** ReviewsServlet for the JettyHttpServer example */
@SuppressWarnings("serial")
public class ReviewsServlet extends HttpServlet {

    /**
     * A method that gets executed when the get request is sent to the
     * HelloServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);

        PrintWriter out = response.getWriter();
        String hotelId = request.getParameter("hotelId");
        String numString = request.getParameter("num");


        if (hotelId == null || numString == null){
            hotelId = "empty";
            numString = "-1";
        }

        int num = Integer.parseInt(numString);

        hotelId = StringEscapeUtils.escapeHtml4(hotelId); // need to "clean up" whatever
        // the user entered

        HotelData data = (HotelData) getServletContext().getAttribute("hotelData");

        out.println(data.getReviewsJson(hotelId, num));

        // Referencing Prof. Engle
        // If we did not call escapeHtml4,
        // if we just used the name (we read from the user) in our response,
        // our site would be prone to cross-site scripting attacks (XSS attacks)

        // You can comment out this line: name = StringEscapeUtils.escapeHtml4(name);
        // and uncomment the line below to disable security feature in the browser
        // response.setIntHeader("X-XSS-Protection", 0);
        // Then in the browser, try specifying the text below instead of the name:
        // ?name=<script>window.open("http://www.usfca.edu/");</script>
        // Did you see what happened? This script opened another page with usfca.edu website!

    }
}
